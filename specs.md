Общие коды ошибок
500: выдать юзеру алерт и восстановить стейт
401: пользователь не залогинен в системе, либо недостаточно прав. По идее, в этом случае надо перезагрузить страницу

1. ~~История (Kodo) в.ч за сегодня~~
  + url: 'lotteries/kodo/participations[/today]'
  + Коды ответа: 200, 401, 500
  ```json
  "participations": {
    "id":1,
    "user_id":1,
    "lottery_id":14,
    "factor":5,
    "win":false, //!!win.nonzero?
    "guns": [1],
    "numbers":[{
      "index": 1,
      "status": "selected"
    }, '...'],
    "circles": [14,15,16]
  }
  ```

3. Магазин
  + url: 'items/'
  + GET:
  // FIXME
  ```json
  {
    "index": 1,
    "preview": "http://placehold.it/115x50",
    "name": "ПП-19 Бизон | Осирис",
    "price": "100.0",
    "startrack": "{{random(true,false)}}",
    "star": "{{random(true,false)}}",
    "weapon_type": "{{random('gun','pistol','rifle')}}",
    "rareness": "{{random('good','normal','bad')}}",
    "classfield": "{{random('red', 'purple', 'yellow', 'blue')}}"
  }
  ```

4. *Оформление покупки*
  + url: 'items/buy'
  + POST:
  ```json
    {
      "offer_link": "offer_link", // из current_user.steam_offer_link
      "items": [ 1, 1, 1, 1, 3, 2, 2]
    }
  ```
  Коды ответа: 200, 401, 404, 422, 500
  + 200: head
  + 404: head // предмет недоступен
  + 422:
  ```json
    {
      "error": "User::LowBalance"
    }
  ```

5. ~~Оформление ставки (Kodo)~~
  + url: 'lotteries/kodo/multi_bet'
  + POST:
  ```json
    {
      "bets": [
        {
          "numbers": [1,2,3,4,5,6,7,8],
          "guns": [1,2,3,4],
        }
      ],
       "factor": 1,
       "circles": 1
    }
  ```
  ИЛИ
  ```json
  {
    "bets": [
      {
        "guns": 3
      }
    ],
    "factor": 1,
    "circles": 1,
    "count": 2
  }
  ```

  + Коды ответа: 200, 401, 422, 500
  + 200: head
  + 422:
  ```json
  {
    "error": "User::LowBalance"
  }
  ```

6. Оформление ставки (Headshot)
  + url: 'lotteries/headshot/bet'
  + POST: только метатеги
  + Коды ответа: 200, 401, 422, 500
  + 200:
  ```json
  {
    win: '100.0'
    // win: '0.0'
  }
  ```
  + 422:
  ```json
  {
    "error": "User::LowBalance"
  }
  ```

7. ~~Обратная связь~~
  + url: 'tickets'
  + POST
  ```json
  {
    ticket: {
      user_name: 'Васек',
      user_email: 'vasya228@mail.ru',
      title: 'lorem ipsum',
      category: 'issues',
      body: '...'
    }
  }
  ```
  + Коды ответа: 200, 401, 422, 500
  + 200: head
  + 422:
  ```json
  {
    errors: {
      ticket: {
        user_nickname: ['укажите никнейм'],
        category: ['категория не найдена'],
        ...
      }
    }
  }
  ```

8. Текущий юзер
  + url: GET /sessions/current_user
  + Код ответа: 200
  ```json
   {
     current_user: {
      "id":1,
      "nickname":"Дверной",
      "steam_id":"76561198069578169",
      "admin_scope_access":true,
      "balance":"1697.0",
      "created_at":"2015-07-04T22:54:10.629Z",
      "steam_offer_link":null
     }
   }
  ```
   ИЛИ
  ```json
   {
     "id":null,
     "nickname":null,
     "steam_id":null,
     ...
   }
  ```

 Эта штука отдается со всеми запросами + по запросу GET '/sessions/current_user'

9. Лотереи kodo
  + url: GET /lotteries/kodo[/last?count=5]
  + Коды ответа: 200, 404
  + Возвращает текущую или последнюю лотерею. Формат ответа:
  ```json
  {
    "lotteries": [
      {
        "id": 1,
        "time_from": "2015-07-04T22:54:10.629Z",
        "time_till": "2015-07-04T22:54:10.629Z",
        "result": {
          gun: 1,
          numbers: [1,2,3,4,5,6,7,8]
        }
        // result: null
        "superprize": '10000.0',
        "superprize_played": false
      }
    ]
  }
  ```

10. ~~часто выпадающие числа и давно не выпадавшие~~
  + url: GET /lotteries/kodo/participations/stats[?last=10]
  + Допускается установка значения last=all, однако желательно использование формы без параметра last
  + Коды ответа: 200, 404
  200:
  ```json
  {
    numbers: {
      2: 100,
      1: 99,
      3: 50,
      ...,
      5: 1
    }
  }
  ```

12. логин через стим
  + GET /auth/steam
  + Код ответа: 200(редирект), 500
  + Возвращает javascript: ```window.location = '#{path}';```, где path - урл стима

13. логаут
  + DELETE /sessions/logout 
  + Код ответа: 200(редирект), 500
  + Возвращает javascript: ```window.location = '/';```, 

  + (здесь и далее DELETE реализуется как 
	POST /sessions/logout с параметром _method: 'delete')

14. Входящий интерфейс робокассы
  + POST /robokassa/signer
  + Код ответа: 200(редирект), 401, 500
  + параметры
  ```json
  {
    amount: сумма в рублях
  }
  ```
  + Возвращает javascript: ```window.location = '#{path}';```, где path - урл робокассы