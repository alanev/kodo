// Modules
var gulp = require('gulp'),

	// file
	concat = require('gulp-concat'),
	open = require('gulp-open'),
	ftp = require('gulp-ftp'),
	zip = require('gulp-zip'),
	clean = require('gulp-clean'),
	flatten = require('gulp-flatten'),
	connect = require('gulp-connect'),

	// css
	postcss = require('gulp-postcss'),
	//autorprefixer
	cssautoprefixer = require('autoprefixer-core'),
	//css4
	csscolor = require('postcss-color-function'),
	cssvars = require('postcss-css-variables'),
	cssmedia = require('postcss-custom-media'),
	cssnot= require('postcss-selector-not'),
	//shortcuts
	cssnested= require('postcss-nested'),
	cssshort = require('postcss-short'),
	cssfocus = require('postcss-focus'),
	//processors
	processors = [
		cssnested(),
		cssmedia(),
		cssvars(),
		cssshort(),
		cssnot(),
		//cssfocus(),
		csscolor(),
		cssautoprefixer()
	],
	//minify
	minify = require('gulp-minify-css'),
	minifyoptions = {
		advanced: false
	},
	
	// js
	uglify = require('gulp-uglify'),

	// image
	imagemin = require('gulp-imagemin'),
	spritesmith = require('gulp.spritesmith'),

	pkg = require('./package.json'),
	ftppass = require('./ftp.json'),

	varEnd = true
	;

// Ftp path
ftppass.remotePath = "/" + pkg.name;

// Options
var src = 'blocks/',
	dest = 'cwd/',
	css = {
		src: [
			src + 'i-*/*.css',
			src + 'g-*/*.css',
			src + 'b-*/*.css',
			src + '**/*.css'
		],
		name:  pkg.name + '.css'
	},
	js = {
		src: [
			src + 'i-*/*.js',
			src + 'g-*/*.js',
			src + 'b-*/*.js',
			src + '**/*.js'
		],
		name: pkg.name + '.js'
	},
	
	sprite = {
		block: 'i-icons',
		src: src + '**/i-*.png',
		css: {
			name: 'i-icons' + '.css',
			dest: src + 'i-icons'
		},
		img: {
			name: 'icons.png',
			dest: src + 'i-icons'
		},
		tmpl: src + 'i-icons' + '/' + 'i-icons' + '.hbs'
	},
	img = {
		name: '**/*.{png,jpg,gif,svg}',
		src: [
			src + '**/*.{png,jpg,gif,svg}',
			'!' + src + '**/i-*.png'
		],
		dest: dest + 'img'
	}
	;

/*-- Upload --*/
gulp.task('ftp',function () {
	gulp.src([
			dest + '**'
		])
		.pipe(ftp(ftppass))
		;
});
gulp.task('zip',function () {
	gulp.src([
			dest + '**'
		],{
			base: __dirname
		})
		.pipe(zip(pkg.name + '.zip'))
		.pipe(ftp(ftppass))
		;
	gulp.src([
			src + '**',
			dest + '**',
			'*',
			'!node_modules',
			'!src',
			'!ftp.json'
		],{
			base: __dirname
		})
		.pipe(zip(pkg.name + '_dev.zip'))
		.pipe(ftp(ftppass))
		;
});

/*-- Html Tasks --*/
gulp.task('html', function () {
	gulp.src([
			dest + '*.htm'
		])
		.pipe(connect.reload())
		;
});

/*-- Css Tasks --*/
gulp.task('css:dev', function () {
	gulp.src(css.src)
		.pipe(concat(css.name))
		.pipe(postcss(processors))
		.pipe(gulp.dest(dest))
		.pipe(connect.reload())
		;
});
gulp.task('css:build', function () {
	gulp.src(css.src)
		.pipe(concat(css.name))
		.pipe(postcss(processors))
		.pipe(minify(minifyoptions))
		.pipe(gulp.dest(dest));
});

/*-- Js Tasks --*/
gulp.task('js:dev', function () {
	gulp.src(js.src)
		.pipe(concat(js.name))
		.pipe(gulp.dest(dest))
		.pipe(connect.reload())
		;
});
gulp.task('js:build', function () {
	gulp.src(js.src)
		.pipe(concat(js.name))
		.pipe(uglify())
		.pipe(gulp.dest(dest))
		;
});

/*-- Images Tasks --*/
gulp.task('img:dev',['img:clean','img:copy']);
gulp.task('img:build',['img:clean','img:min']);
gulp.task('img:min',function () {
	gulp.src(img.src)
		.pipe(flatten())
		.pipe(imagemin({
			optimizationLevel: 5,
			progressive: true
		}))
		.pipe(gulp.dest(img.dest))
		;
});
gulp.task('img:copy',function () {
	gulp.src(img.src)
		.pipe(flatten())
		.pipe(gulp.dest(img.dest))
		;
});
gulp.task('img:clean',function () {
	gulp.src(img.dest)
		.pipe(clean())
		;
});

/*-- Sprite Tasks --*/
gulp.task('sprite',function () {
	var spriteData = gulp.src(sprite.src)
		.pipe(spritesmith({
			imgName: sprite.img.name,
			cssName: sprite.css.name,
			cssTemplate: sprite.tmpl,
			padding: 5
		}));
	spriteData.img
		.pipe(gulp.dest(sprite.img.dest));
	spriteData.css
		.pipe(gulp.dest(sprite.css.dest));
});

gulp.task('done',function () {
	console.log('done');
});

/*-- Dev Tasks --*/
var server = {
	root: dest,
	port: 8001,
	livereload: true
};
gulp.task('open',function () {
	gulp.src(dest + 'index.htm')
		.pipe(open('http://localhost:' + server.port + '/index.htm', { app: 'firefox' }))
		.pipe(open('<%= file.cwd %>', { app: 'explorer' }))
		;
});
gulp.task('watch', ['css:dev','js:dev'], function () {

	connect.server(server);
	gulp.watch(dest + '**/*.htm', ['html']);
	
	gulp.watch(src + '**/*.css', ['css:dev']);
	gulp.watch(src + '**/*.js', ['js:dev']);
})
gulp.task('dev', ['watch','open']);

/*-- Dev Build --*/
gulp.task('build', ['img:build','css:build','js:build']);