app.controller('betsCtrl', function ($scope, $http) {
	var bets = this;
	this.type = 'kodo';
	this.shown = false;
	this.kodo = [];
	this.headshot = [];
	this.guns = guns;
	
	// get kodo bets
	$http.get(url.host + 'lotteries/kodo/participations' + url.user)
		.success(function(data, status){
			bets.kodo = data.participations;
			
			angular.forEach(bets.kodo, function(rate){
				rate.numbers = $.grep(rate.numbers, function( n , i){
					return ( n.status === 'matched' || n.status === 'selected' )
				});
			});
		});
	
	// get headshot bets
	$http.get(url.host + 'lotteries/headshot/participations' + url.user)
		.success(function(data, status){
			bets.headshot = data.participations;
		});
	
	$scope.$on('betsToggle', function(event, args){
		bets.shown = args;
	});
})
.directive('bets', function(){
	return {
		templateUrl: 'views/bets.htm'
	};
});