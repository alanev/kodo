app.directive('currentTime', ['$interval', function ($interval) {
		return function (scope, element) {
			function updateTime() {
				var date = new Date();
				element.text(date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds());
			}
			$interval(updateTime, 1000);
		}
	}])
	.directive('currentGmt', function () {
		return function (spoce, element) {
			var date = new Date();
			element.text(date.getTimezoneOffset()/60);
		};
	});