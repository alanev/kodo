app.controller('lotteryCtrl', function ($http, $rootScope, $filter) {
	var lottery = this;
	// Data
	// common
	this.bet = 0;
	this.guns = guns;
	this.type = 'hand';
	this.numbers = [];
	for (var i = 0; i < 30; i++) {
		this.numbers.push({
			index: i + 1,
			selected: false
		});
	};
	// bets
	this.hand = {
		bets: [{
			numbers: [],
			guns: [],
		}],
		extra: {
			factor: {
				available: [1, 2, 3, 4, 5, 10, 20, 50, 100],
				value: 1
			},
			circles: {
				available: [1, 2, 3, 4, 5, 10, 20, 30],
				value: 1
			}
		}
	};
	this.multi = {
		extra: {
			bets: {
				available: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 40, 60, 80, 90, 100],
				value: 10
			},
			guns: {
				available: [1, 2, 3, 4],
				value: 1
			},
			factor: {
				available: [1, 2, 3, 4, 5, 10, 20, 50, 100],
				value: 1
			},
			circles: {
				available:  [1, 2, 3, 4, 5, 10, 20, 30],
				value: 1
			}
		}
	}
	
	// Methods
	// fields
	this.toggleNumber = function (index) {
		this.toggleField('numbers', index, 8);
	};
	this.isNumber = function (numb) {
		return this.isField('numbers', numb);
	};
	this.toggleGun = function (index) {
		this.toggleField('guns', index, 4);
	};
	this.isGun = function (numb) {
		return this.isField('guns', numb);
	};
	this.toggleField = function (fieldName, index, max) {
		var field = this.hand.bets[this.bet][fieldName];
		if (field.indexOf(index) != -1) {
			field.splice(field.indexOf(index), 1); //remove numbers from array
		} else if (field.length < max) {
			field.push(index); //add number to array
		}
	};
	this.isField = function (name, numb) {
		return (this.hand.bets[this.bet][name].indexOf(numb) != -1);
	};
	// bets
	this.cleanBet = function () {
		this.setBet([], []);
	};
	this.cleanBets = function (numb) {
		this.bet = -1;
		this.hand.bets = [];
		this.addBet();
	};
	this.setBet = function (numbers, guns) {
		this.hand.bets[this.bet].numbers = numbers;
		this.hand.bets[this.bet].guns = guns;
	};
	this.addBet = function () {
		this.bet++;
		this.hand.bets.push({
					numbers: [],
					guns: [],
				});
	};
	this.removeBet = function (index) {
		if(this.hand.bets.length > 1){
			this.hand.bets.splice(index, 1);
			this.bet--;
		}
	};
	//send
	this.send = function () {
		// validation
		var valid = true;
		angular.forEach(this[this.type].bets, function (value, key){
			if(value.numbers.length === 0 || value.guns.lengths === 0){
				valid = false;
			}
		});
		if(valid){
			// preparation
			var request = angular.copy(this[this.type]);
			angular.forEach(request.extra, function (value, key) {
				request[key] = value.value;
			});
			delete request.extra;
			// sending
			$.post(url.host + 'lotteries/kodo/multi_bet' + url.user, request)
				.complete(function(x){
					if(x.status === 200){
						$rootScope.$broadcast('moveToggle', true);
						lottery.cleanBets();
					}
					console.log(x);
				});
		}else{
			console.log('fail');
		}
	};
	// auto
	this.auto = {};
	this.autoRandom = function () {
		this.setBet(randomArray(0, 30, 8), [randomNumber(0, 4)]);
	};
	this.autoEven = function () {
		this.setBet(randomArray(0, 30, 8, 'even'), [randomNumber(0, 4)]);
	};
	this.autoOdd = function () {
		this.setBet(randomArray(0, 30, 8, 'odd'), [randomNumber(0, 4)]);
	};
	this.autoFirst = function () {
		this.setBet(randomArray(0, 15, 8), [randomNumber(0, 4)]);
	};
	this.autoLast = function () {
		this.setBet(randomArray(15, 30, 8), [randomNumber(0, 4)]);
	};
	// extra
	this.setExtraValue = function (name, value) {
		var extra = this[this.type].extra[name];
		extra.value = value;
	};
	this.extraMore = function (name) {
		this.extraChange(name,+1);
	};
	this.extraLess = function (name) {
		this.extraChange(name,-1);
	};
	this.extraChange = function (name, shift) {
		var extra = this[this.type].extra[name],
			index = extra.available.indexOf(extra.value);
		if( (index === 0 && shift < 0) || (index === extra.available.length - 1 && shift > 0) ){
			extra.value = extra.available[index];
		}else{
			extra.value = extra.available[index + shift];
		}
	};
	
	// sum
	this.sum = function () {
		if(this.type === 'hand'){
			return 30 * this.hand.bets.length * this.hand.extra.factor.value * this.hand.extra.circles.value;
		}else if(this.type === 'multi'){
			return 30 * this.multi.extra.bets.value * this.multi.extra.guns.value * this.multi.extra.factor.value * this.multi.extra.circles.value;
		}
	};
})
.filter('numb', function () {
	return function(input){
		input = input || '';
		var output = '';
		output = angular.uppercase(input);
		console.log(output);
		return output;
	};
});

// Extra functions
function randomArray(min, max, sum, condition) {
	var arr = [];

	for (var i = 0; i < sum; i++) {
		var tmp = randomNumber(min, max, condition);
		var addElement = true;
		for (var j = 0; j < arr.length; j++) {
			if (arr[j] === tmp) {
				addElement = false;
				i--;
			}
		}
		if (addElement) {
			arr[i] = tmp;
		}
	}

	return arr;
}
function randomNumber(min, max, condition) {
	var numb = randomInt(min, max);;
	if (condition === 'odd') {
		while ((numb & 1) != 1) {
			numb = randomInt(min, max);
		}
	} else if (condition === 'even') {
		while ((numb & 1) != 0) {
			numb = randomInt(min, max);
		}
	}
	return numb;
}
function randomInt(min, max) {
	return Math.floor(Math.random() * (max - min) + min);
}
function oddOrEven(target) {
	return (target & 1) ? 'odd' : 'even';
}