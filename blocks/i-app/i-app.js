var app = angular.module('lotteryApp', ['ngRoute']);

//urls for requests
var url = {
	host: 'http://steamlotto.ru/',
	user: '?current_user_id=1'
};

//routing
app.config(function($routeProvider){
	$routeProvider
		.when('/',{
			templateUrl: 'views/lottery.htm'
		})
		.when('/callback',{
			templateUrl: 'views/callback.htm'
		})
		.when('/archive',{
			templateUrl: 'views/archive.htm'
		})
		.when('/history',{
			templateUrl: 'views/history.htm'
		})
		.when('/store',{
			templateUrl: 'views/store.htm'
		})
		.when('/headshot',{
			templateUrl: 'views/headshot.htm'
		})
		.otherwise({
			templateUrl: 'views/error.htm'
		});
});