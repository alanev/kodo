app.controller('bannerCtrl', function ($scope, $rootScope, $http, $interval) {
	var banner = this;
	this.move = false;
	this.time = function(){
		var r = banner.time_till - new Date();
		r = (r-r%1000)/1000;
		if(r > 0){
			console.log(r);
			if(r === 280){
				$rootScope.$broadcast('dataCtrl', true);
				console.log(r);
			}
			return (r-r%60)/60 + ':' + r%60;
		}
	};

	$http.get(url.host + 'lotteries/kodo' + url.user)
		.success(function (data, status) {
			banner.time_till = new Date(data.lotteries[0].time_till);
		});

	$scope.$on('moveToggle', function (event, args) {
		banner.move = args;
	});
});