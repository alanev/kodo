var guns = [
	{
		"preview": "gun1.png",
		"title": "Desert Eagle",
		"subtitle": "«Пламя»",
		"theme": "yellow",
		"selected": false
	},
	{
		"preview": "gun2.png",
		"title": "AK-47",
		"subtitle": "«Вулкан»",
		"theme": "purple",
		"selected": false
	},
	{
		"preview": "gun3.png",
		"title": "AWP ASIMOV",
		"subtitle": "«Азимов»",
		"theme": "red",
		"selected": false
	},
	{
		"preview": "gun4.png",
		"title": "«Original Knife»",
		"theme": "blue",
		"selected": false
	}
];