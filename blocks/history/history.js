app.controller('historyController', function ($http, $scope) {
	var history = this;
	this.filter = {};
	this.filter.winning = undefined;
	this.filter.circle = undefined;
	this.filter.view = 'full';
	this.rates = [];
	this.circles = [];
	this.guns = guns;
	
	// carousel
	this.shiftSize = 320;
	this.shiftNumb = 0;
	this.shift = function () {
		return this.shiftSize * this.shiftNumb;
	};
	this.next = function () {
		var length = angular.element('.history__full').length - 3;
		if (this.shiftNumb > -(length)) {
			this.shiftNumb--;
		}
	};
	this.prev = function () {
		if (this.shiftNumb < 0) {
			this.shiftNumb++;
		}
	};
	this.resetShift = function () {
		this.shiftNumb = 0;
	};
	$scope.$watch('history.filter.winning', function (newValue, oldValue) {
		history.resetShift();
	});
	$scope.$watch('history.filter.cirles', function (newValue, oldValue) {
		history.resetShift();
	});
	$http.get(url.host + '/lotteries/kodo/participations' + url.user)
		.success(function(data, status){
			if (status === 200) {
				history.rates = data.participations;
				angular.forEach(history.rates, function (rate, key) {
					
					// count cirles
					angular.forEach(rate.circles, function (value) {
						if (history.circles.indexOf(value) === -1) {
							history.circles.push(value);
						}
					});
					
					// count winning
					rate.winning = $.grep(rate.numbers, function( n , i){
						return ( n.status === 'winning' )
					}).length;
					
					// change guns array to gun value
					rate.gun = rate.guns[0];
					delete rate.guns
				});
				history.circles.sort();
				console.log(history.rates);
			} else if (status === 500) {
				console.log(status, data);
			}
		});
});