app.controller('userCtrl', function ($location, $http) {
	this.id = 1;
	this.loged = true;
	this.balance = 600;
	this.login = function () {
		$http.get(url + 'auth/steam')
			.success(function (data, status) {
				if (status === 200) {
					this.loged = false;
				}
				console.log(data, status);
			});
		console.log('login');
	};
	this.loginUrl = url + 'auth/steam';
	this.logout = function () {
		$http.post(url + 'sessions/logout', { _method: 'delete', current_user_id: 1 })
			.success(function (data, status) {
				if (status === 200) {
					this.loged = false;
				}
			});
	};
	this.buy = function (sum) {
		if (sum > 0 && sum <= this.balance) {
			this.balance -= sum;
			return true;
		} else {
			return false;
		}
	};
	this.url = function () {
		return $location.$$path;
	};
});