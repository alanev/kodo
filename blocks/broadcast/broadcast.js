app.controller('broadcastCtrl', function ($http, $scope, $interval) {
	var broadcast = this;
	this.shown = false;
	this.auto = true;

	$scope.$on('dataCtrl', function (event, args) {
		broadcast.shown = args;
	});
})
.directive('broadcast', function () {
	return {
		templateUrl: 'views/broadcast.htm'
	};
});