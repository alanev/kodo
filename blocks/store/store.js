app.controller('storeCtrl', function ($http,$scope) {
	var store = this;
	// guns
	store.guns = [];
	store.settings = [];
	// filterring
	this.filter = {};
	this.toggle = function(target,value){
		this.filter[target] = value;
	};
	this.toggleSt = function(){
		if(this.filter.st){
			this.filter.st = undefined;
		}else{
			this.filter.st = true;
		}
	}
	this.toggleStar = function(){
		if(this.filter.star){
			this.filter.star = undefined;
		}else{
			this.filter.star = true;
		}
	}
	this.selected = function(name){
		if(name){
			return name;
		}else{
			return 'Все';
		}
	};
	// inventory
	this.example = [{},{},{},{},{},{},{},{},{},{},{},{}];
	this.inventory = [];
	this.total = new Number;
	this.addGun = function(data){
		this.inventory.push(data);
		this.total += data.price;
		
		if(this.example.length > 0){
			this.example.splice(0, 1);
		}
	};
	this.removeGun = function(index){
		this.total -= this.inventory[index].price;
		this.inventory.splice(index, 1);
		
		if(this.example.length < 12){
			this.example.push({});
		}
	};
	this.buy = function (sum){
		if($scope.$parent.user.buy(sum)){
			this.inventory = [];
			this.total = 0;
		}
	};
	// best
	this.best = [];

	$http.get('http://beta.json-generator.com/api/json/get/Leg-LWu?indent=0')
		.success(function (data) {
			store.guns = data.guns;
			store.best = data.best;
			store.settings = data.settings;
		});
});