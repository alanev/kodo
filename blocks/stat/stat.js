app.directive('stat', function () {
	return {
		templateUrl: 'views/stat.htm'
	}
}).controller('statCtrl', function ($scope, $rootScope, $http, $filter) {
	var stat = this;
	this.type = 'kodo';
	
	// cirles
	this.circle = 17464;
	this.circles = [17464, 17465];
	this.circlesShow = false;
	
	// often
	this.oftenType = 'all';
	this.oftenAll = [];
	this.oftenLast = [];
	
	// request
	$http.get(url.host + '/lotteries/kodo/participations/stats')
		.success(function (data, status) {
			angular.forEach(data.numbers, function (value, key) {
				if (key > 0) {
					stat.oftenAll.push({
						number: key,
						value: value
					});
				}
			});
		});
	$http.get(url.host + '/lotteries/kodo/participations/stats', { last: 10 })
		.success(function (data, status) {
			angular.forEach(data.numbers, function (value, key) {
				if (key > 0) {
					stat.oftenLast.push({
						number: key,
						value: value
					});
				}
			});
		});
	
	// methods
	this.broadcast = function () {
		$rootScope.$broadcast('dataCtrl', true);
	};
	this.bets = function () {
		$rootScope.$broadcast('betsToggle', true);
	};
});